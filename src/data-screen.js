import React, {Component} from 'react';
import {
  View, 
  Text,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { Item, Input, Icon, StyleProvider } from 'native-base';

import { db } from './components/database'
import styles from './models/calendar-style'
import getTheme from '../styles/native-base-theme/components';
import material from '../styles/native-base-theme/variables/material';
import { Actions } from 'react-native-router-flux';
import ItemComponent from './components/items'




export default class DataScreen extends Component {
  constructor(props) {
    super(props);
      this.props.myDay;
  }

  state = {
    items: []
  }

  componentDidMount() {
    let day = this.props.myDay;
    if (day) {
      db.ref(`${day.year}/${day.month}/${day.day}`).once('value').then(function(snapshot) {
        console.log(snapshot.val());
        let items = Object.values(snapshot.val());
        this.setState({items});
      });
    }
  }

  renderPressedDate() {
    day = this.props.myDay;
    if (day) {
      let date = Number(day.day);
      let month = Number(day.month);
      let year = Number(day.year);
      return `${date}/${month}/${year}`;
    }
  }

  renderTopBar() {
    return (
      <View style={styles.header}>
        <TouchableOpacity 
          onPress={() => Actions.calendar()}
          style={styles.headerLeft}
        >
          <Text style={styles.headerText}>Cancel</Text>
        </TouchableOpacity>

        <View style={styles.headerCenter}>
          <Text style={styles.centerHeaderText}>
            { this.renderPressedDate() }
          </Text>
        </View>
      </View>
    );
  }

  renderMain() {
    return (
      <View style={styles.container}>
        {alert(this.state.items)}
        {
          this.state.items.length > 0
          ? <ItemComponent items={this.state.items} />
          : <Text>No items</Text>
        }

      </View>
    );
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <View style={{ flex: 1 }}>
          { this.renderTopBar() }
          { this.renderMain() }
        </View>
      </StyleProvider>
    )
  }
} 