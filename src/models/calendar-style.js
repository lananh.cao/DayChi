import { 
    StyleSheet,
} from 'react-native';

module.exports = StyleSheet.create({
  header: {
    backgroundColor: '#5C6370',
    flexDirection: 'row',
    padding: 10
  },
  container: {
    padding: 20,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 90
  },
  headerLeft: {
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    position: 'relative',
  },
  headerRight: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    
  },
  headerCenter: {
    flexDirection: 'row',
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    position: 'relative',
  },
  centerHeaderText: {
    fontSize: 50, 
    fontFamily: 'Dense-Regular'
  },
  headerText: {
    fontSize: 30, 
    fontFamily: 'Dense-Regular'
  },
});