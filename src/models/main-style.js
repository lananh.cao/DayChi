import { 
    StyleSheet,
} from 'react-native';

module.exports = StyleSheet.create({
    barContainer: {
      backgroundColor: '#5C6370',
      flex: 10
    },
    mainContainer: {
      padding: 20,
      backgroundColor: '#282C34',
      flexDirection: 'column',
      justifyContent: 'center',
      flex: 90
    },
    inputArea: {
      paddingBottom: 20,
      paddingHorizontal: 10
    },
    input: {
      height: 40,
      backgroundColor: 'rgb(255,255,255)',
      marginBottom: 20,
      color: 'rgb(34,49,63)',
      paddingHorizontal: 10
    },
    buttonContainer: {
      padding: 40,
      paddingVertical: 10,
    },
    buttonText: {
      textAlign: 'center',
      color: '#45E09C'
    },

    dataScreen: {
        backgroundColor: '#282C34',
        flex: 1, 
        flexDirection: 'row',
    },
    oneDayExpense: {
        backgroundColor: 'yellow',
        width: 140,
        height: 170,
        margin: 25
    }
  });