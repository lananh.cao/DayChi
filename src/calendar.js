import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux'; 
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { Container, Header, Content, Icon } from 'native-base';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

import styles from './models/calendar-style'

export default class MyCalendar extends Component {
  render() {
    return (
      <View>
        <View style={styles.header}>
          <TouchableOpacity 
            onPress={() => Actions.inputscreen()}
            style={styles.headerLeft}
          >
            <Text style={styles.headerText}>Cancel</Text>
          </TouchableOpacity>

          <View style={styles.headerCenter}>
              <Text style={styles.centerHeaderText}>Calendar</Text>
          </View>
          
          <View style={styles.headerRight}>
              <Text style={styles.headerText}>Today</Text>
          </View>
        </View>

        <Calendar
          // Callback which gets executed when visible months change in scroll view. Default = undefined
          onVisibleMonthsChange={(months) => {console.log('now these months are visible', months);}}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={50}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={50}
          // Enable or disable scrolling of calendar list
          scrollEnabled={true}
          // Enable or disable vertical scroll indicator. Default = false
          showScrollIndicator={true}
          // Initially visible month. Default = Date()
          current={Date()}
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={'2014-01-01'}
          // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
          maxDate={undefined}
          // Handler which gets executed on day press. Default = undefined
          onDayPress={(day) => {Actions.datascreen({myDay: day})}}
          // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
          monthFormat={'MMM yyyy'}
          // Handler which gets executed when visible month changes in calendar. Default = undefined
          onMonthChange={(month) => {console.log('month changed', month)}}
          // Do not show days of other months in month page. Default = false
          hideExtraDays={true}
          // day from another month that is visible in calendar page. Default = false
          disableMonthChange={true}
          // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
          firstDay={1}
          // Hide day names. Default = false
          hideDayNames={true}
          // Show week numbers to the left. Default = false
          showWeekNumbers={false}
          // Handler which gets executed when press arrow icon left. It receive a callback can go back month
          onPressArrowLeft={substractMonth => substractMonth()}
          // Handler which gets executed when press arrow icon left. It receive a callback can go next month
          onPressArrowRight={addMonth => addMonth()}
        />
      </View>
    );
  }
}