import React, { Component } from 'react';
import { 
  View,
} from 'react-native';
import { Button, Text, Item, Input, Icon, StyleProvider } from 'native-base';
import { Actions } from 'react-native-router-flux';

import { db } from './components/database'
import Paid from './models/paid';
import Currency from './components/currency-buttons';
import styles from './models/main-style'
import getTheme from '../styles/native-base-theme/components';
import material from '../styles/native-base-theme/variables/material';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      price: 0,
      paidList: [],
      date: '',
      month: '',
      year: '',
      isDisabled: true
    };
  }

  uploadData(name, price) {
    let array = [];
    array.push({
      name: this.state.title,
      price: this.state.price
    })

    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let hour = new Date().getHours();
    let min = new Date().getMinutes();
    let sec = new Date().getSeconds();

    db.ref(year + '/' + month + '/' + date + '/').set({
      time: `${hour}:${min}:${sec}`,
      name: this.state.title,
      price: this.state.price,
    });
  }

  adding(money) {
    let total = parseInt(this.state.price, 10) + money;
    this.setState({price: total.toString() }); 
    
  }

  savePaid() {
    this.resetInput()
    let paid = new Paid();
    paid.title = this.state.title;
    paid.price = this.state.price;
    
    this.setState({paidList: [...this.state.paidList, paid]});
    this.uploadData(this.state.title, this.state.price);
  }

  renderCurrentDate() {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    return `${date}/${month}/${year}`;
  }

  renderTopBar() {
    return (
      <View style={styles.barContainer}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 50, fontFamily: 'Dense-Regular' }}>
            { this.renderCurrentDate() }
          </Text>
        </View>
      </View>
    );
  }

  renderPriceIcon() {
    if (this.state.isDisabled) {
      return (
        <Icon
          onPress={() => this.setState({ isDisabled: false })}
          name='pencil' type='Foundation'
        />
      );
    } else {
      return (
        <Icon onPress={() => this.setState({ isDisabled: true })} name='cancel' type='MaterialIcons' />
      );
    }
  }

  resetInput()
  {
    this.setState({title: ""});
    this.setState({price: 0});
  }

  formatPrice() {
    let price = this.state.price;
    console.log(price);

    return `${this.state.price.toString()} VND`;
  }

  renderMain() {
    return (
      <View style={styles.mainContainer}>

        <View style={{ paddingBottom: 20}}>
          <Item style={{ borderColor: 'rgba(255, 255, 255, 0.34)' }}>
            <Input
              placeholder='Sản phẩm vừa mua'
              value={this.state.title}
              onChangeText={(value) => this.setState({title: value})}
            />
          </Item>
        </View>

        <View style={{ paddingBottom: 20}}>
          <Item disabled={this.state.isDisabled}>
            <Input
              disabled={this.state.isDisabled}
              placeholder='Giá tiền'
              value={this.formatPrice()}
              onChangeText={(value) => this.setState({price: value})}
            />
            { this.renderPriceIcon() }
          </Item>
        </View>
        <Currency adding={this.adding.bind(this)}></Currency>

        <View style={{
          justifyContent: 'center',
          flexDirection: 'row',
          alignItems: 'center'
        }}>
          <Button rounded iconLeft
            onPress={this.onSavePressed.bind(this)}
            style={{ backgroundColor: '#45E09C'}} 
          >
            <Icon name='check' type='MaterialIcons' />
            <Text>Lưu</Text>
          </Button>
        </View>

      </View>
    );
  }

  onSavePressed() {
    this.savePaid();
    Actions.calendar();
  }

  render() {
    return(
      <StyleProvider style={getTheme(material)}>
        <View style={{ flex: 1 }}>
          { this.renderTopBar() }
          { this.renderMain() }
        </View>
      </StyleProvider>
    );
  }
}
