import React from 'react';
import { 
  StyleSheet,
} from 'react-native';
import { Button, Text } from 'native-base';

export default MoneyButton = ({ buttonColor, onPress, children }) => {
  return (
    <Button rounded 
        onPress={onPress}
        style={[styles.buttonContainer, { backgroundColor: '#56B6C2' }]}
    >
        <Text style={styles.buttonText}>
            {children} VND
        </Text>
    </Button>
    );
  };
  
const styles = StyleSheet.create({
  buttonContainer: {
    width: 50, 
    height: 40, 
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1
  },
  buttonText: {
    textAlign: 'center',
    color: '#FFFFFF'
  },
});
