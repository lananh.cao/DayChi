import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { Grid, Col } from 'native-base';

import MoneyButton from './money-button-componentize';

export default Currency = ({ adding }) => {
  return (
    <View style={{ flex: 1 }}>
      <Grid>
        <Col>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(100)}>
              100
            </MoneyButton>
          </View>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(1000)}>
              1000
            </MoneyButton>
          </View>

          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(10000)}>
              10 000
            </MoneyButton>
          </View>

          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(100000)}>
              100 000
            </MoneyButton>
          </View>
        </Col>

        <Col>
          <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5
              }}
            >
            <MoneyButton onPress={() => adding(200)}>
              200
            </MoneyButton>
          </View>
          <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5
              }}
            >
            <MoneyButton onPress={() => adding(2000)}>
              2000
            </MoneyButton>
          </View>
          <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5
              }}
            >
            <MoneyButton onPress={() => adding(20000)}>
              20 000
            </MoneyButton>
          </View>
          <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5
              }}
            >
            <MoneyButton onPress={() => adding(200000)}>
              200 000
            </MoneyButton>
          </View>
        </Col>
        <Col style={{  }}>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(500)}>
              500
            </MoneyButton>
          </View>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(5000)}>
              5000
            </MoneyButton>
          </View>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(50000)}>
              50 000
            </MoneyButton>
          </View>
          <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5
            }}
          >
            <MoneyButton onPress={() => adding(500000)}>
              500 000
            </MoneyButton>
          </View>
        </Col>
      </Grid>
    </View>
  );
}