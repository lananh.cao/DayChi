import React, {Component} from 'react';
import { View } from 'react-native';
import {
  Scene,
  Router,
  Actions,
} from 'react-native-router-flux';

import App from './app'
import DataScreen from './data-screen'
import MyCalendar from './calendar'

const MyRouter = () => (
  <Router>
    <Scene key="root">
      <Scene 
        key="inputscreen" 
        component={App}
        hideNavBar={true} 
        
      />
      <Scene 
        key="calendar" 
        component={MyCalendar} 
        hideNavBar={true} 
        
      />
      <Scene 
        key="datascreen" 
        component={DataScreen} 
        hideNavBar={true} 
        initial
      />
    </Scene>
  </Router> 
); 
export default MyRouter;